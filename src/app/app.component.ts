import { Component } from '@angular/core';
import { AuthService } from './core/auth/auth/auth.service';

@Component({
  selector: 'app-root, app-placki',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Diebold Music';
  navbarOpen = false;

  login(){
    this.auth.login()
  }

  constructor(private auth:AuthService){

  }
}
