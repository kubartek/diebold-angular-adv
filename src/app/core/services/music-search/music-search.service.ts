import { EventEmitter, Inject, Injectable, Optional } from '@angular/core';
import { Album, AlbumsSearchResponse } from '../../model/search';
import { ALBUMS_INIT_DATA, API_URL } from '../../tokens';

import { HttpClient, HttpErrorResponse } from '@angular/common/http'
import { catchError, concatAll, concatMap, exhaustMap, map, mergeAll, mergeMap, startWith, switchMap, tap } from 'rxjs/operators';
import { AsyncSubject, BehaviorSubject, concat, EMPTY, from, merge, ReplaySubject, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MusicSearchService {
  // /* Queries */
  private queries = new ReplaySubject<string>(5)
  queryChanges = this.queries.asObservable()

  // TODO: Error / NotificationService.notify(error)
  private errorNotifications = new ReplaySubject<string>(1, 10_000)
  recentErorrs = this.errorNotifications.asObservable()

  private results = new BehaviorSubject<Album[]>([])
  resultsChanges = this.results.asObservable()

  constructor(
    private http: HttpClient,
    @Inject(API_URL) private api_url: string,
    @Optional() @Inject(ALBUMS_INIT_DATA) private initialResults: Album[]) {
    (window as any).subject = this.results

    this.results.next(initialResults || [])
    this.errorNotifications.subscribe(console.error)
    this.queries.pipe(
      // tap(x => console.log(x)),
      switchMap(query => this.makeSearchRequest(query).pipe(
        catchError(error => {
          this.errorNotifications.next(error); return EMPTY
        })
      )),
      // tap(x => console.log(x)),
    ).subscribe(this.results)
  }

  // /* Commands */
  searchAlbums(query = 'batman') {
    this.queries.next(query)
  }

  // /* Effects */
  makeSearchRequest(query: any) {
    return this.http.get<AlbumsSearchResponse>(
      `${this.api_url}/search`, {
      params: {
        type: 'album', query
      }
    }).pipe(map(res => res.albums.items));
  }

  getAlbumById(id: string) {
    return this.http.get<Album>(`${this.api_url}/albums/${id}`)
  }
}