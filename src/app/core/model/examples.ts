interface Track {
  id: string;
  name: string;
  type: 'track',
  duration: number
}

export interface Entity {
  id: string,
  name: string
}

 interface Playlist /* extends Entity */{
  id: string;
  name: string;
  type: 'playlist'
  /**
   * IS playlist public?
   */
  public: boolean;
  description: string;
  tracks?: Track[]
}

const playlist: Playlist = {
  id: '123',
  type: 'playlist',
  description: '',
  name: '123',
  public: true,
  tracks: []
}

const searchresults: (Track | Playlist)[] = []
const result = searchresults[0]

if (result.type == 'playlist') {
  result.tracks?.length
} else {
  result.duration
}

switch (result.type) {
  case 'playlist': result.tracks; break;
  case 'track': result.duration; break;
}

// if (playlist.tracks) { playlist.tracks.length }
// const l = playlist.tracks ? playlist.tracks.length.toString() : 'No items'
// const x = playlist.tracks && playlist.tracks.length
// const y = playlist.tracks?.length
// const z = playlist.tracks!.length
// function xyz(p: Playlist) {
//   if (!p.tracks) { return; } return p.tracks.length
// }



// interface Entity {
//   id: string | number,
//   name: string
// }
// const e:Entity = {} as Entity;
// e.id.toString()

// if(typeof e.id == 'string'){
//   e.id.trim()
// }else{
//   e.id.toExponential()
// }


// interface Vector { x: number, y: number, length: number }
// interface Point { x: number, y: number }

// let v: Vector = { x: 123, y: 123, length: 123 };
// let p: Point = { x: 123, y: 123 };

// // p = v; // OK
// // v = p; // Property 'length' is missing in type 'Point' but required in type 'Vector'.
