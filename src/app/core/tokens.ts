import { InjectionToken } from '@angular/core';
import { Playlist } from './model/playlist';

export const PLAYLISTS_INIT_DATA = new InjectionToken<Playlist[]>('PLAYLISTS_INIT_DATA - Initial playlists data');
export const ALBUMS_INIT_DATA = new InjectionToken<Playlist[]>('ALBUMS_INIT_DATA - Initial albums data');
export const API_URL = new InjectionToken<Playlist[]>('API_URL - REST API ROOT URL');



export const PlaylistsSource = new InjectionToken<PlaylistsSource>('PlaylistsSource')

export interface PlaylistsSource {
  getPlaylists(): Playlist[]
}
