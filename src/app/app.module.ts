import { BrowserModule } from '@angular/platform-browser';
import { ApplicationRef, DoBootstrap, NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { playlistsMock } from './core/mocks/playlists.mock';
import { ALBUMS_INIT_DATA, PLAYLISTS_INIT_DATA } from './core/tokens';
import { albumsMock } from './core/mocks/albums.mock';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    // SuperHiperExtraWiget2
    BrowserModule,
    AppRoutingModule,
    CoreModule
  ],
  providers: [
    {
      provide: PLAYLISTS_INIT_DATA,
      useValue: playlistsMock
    },
    {
      provide: ALBUMS_INIT_DATA,
      useValue: albumsMock
    }
  ],
  // bootstrap: [AppComponent/* HeaderComponent, FooterComponent, ... */]
})
export class AppModule implements DoBootstrap{
  ngDoBootstrap(appRef: ApplicationRef): void {
    // getConfigFromServer().then(() => ...
    appRef.bootstrap(AppComponent,'app-root')
  }
}
